import { Injectable } from '@angular/core';
import {Subject, Subscribable} from 'rxjs';
import {Asset} from '../../definition/asset';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {

  assetsSubject = new Subject<Asset[]>();

  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/assets`;

  constructor(private http: HttpClient) { }

  emmitAssets(content: Asset[]) {
    this.assetsSubject.next(content);
  }

  getAssets(request = {}): Subscribable<{data: Object}&any>{
    const strRequest: string = JSON.stringify(request);
    return this.http.get(`${this.url}`, {params: {request : strRequest}});
  }

  getAsset(name: string): Subscribable<{ data: Asset }>{
    return this.http.get(`${this.url}/${name}`);
  }

  unreportGroupAsset(names: string[]): Subscribable<any>{
    return this.http.post(`${this.url}/unreport`, {data : names});
  }
  reportGroupAsset(data: Omit<Asset['exclusion'], 'excludeBy'|'isExclude'>&string[]): Subscribable<any>{
    return this.http.post(`${this.url}/report`, {data});
  }
}
