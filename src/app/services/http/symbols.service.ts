import { Injectable } from '@angular/core';
import {Subject, Subscribable} from 'rxjs';
import {ISymbol} from '../../definition/ISymbol';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SymbolsService {

  symbolsSubject = new Subject<ISymbol[]>();

  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/symbols`;

  constructor(private http: HttpClient) { }

  emmitSymbols(content: ISymbol[]) {
    this.symbolsSubject.next(content);
  }

  getSymbols(request = {}): Subscribable<{data: Object}&any>{
    const strRequest: string = JSON.stringify(request);
    return this.http.get(`${this.url}`, {params: {request : strRequest}});
  }

  getSymbol(name: string): Subscribable<{ data: ISymbol }>{
    return this.http.get(`${this.url}/${name}`);
  }

  unreportGroupSymbol(names: string[]): Subscribable<any>{
    return this.http.post(`${this.url}/unreport`, {data : names});
  }

  resetMoyennes(): Subscribable<any>{
    return this.http.get(`${this.url}/resetMoyennes`);
  }

  reportGroupSymbol(data: Omit<ISymbol['exclusion'], 'excludeBy'|'isExclude'>&string[]): Subscribable<any>{
    return this.http.post(`${this.url}/report`, {data});
  }
}
