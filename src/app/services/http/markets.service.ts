import { Injectable } from '@angular/core';
import { Subject, Subscribable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Market} from '../../definition/market';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarketsService {

  marketsSubject = new Subject<Market[]>();
  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/markets`;

  constructor(private http: HttpClient) { }

  emmitMarkets(content: Array<Market&any>) {
    this.marketsSubject.next(content);
  }

  getMarkets(request = {}): Subscribable<{data: any[], metadata?: any}>{
    const strRequest: string = JSON.stringify(request);
    return this.http.get(`${this.url}`, {params: {request : strRequest}});
  }

  getMarket(name: string): Subscribable<{ data: Market }>{
    return this.http.get(`${this.url}/${name}`);
  }

  unreportGroupMarket(names: string[]): Subscribable<any>{
    return this.http.post(`${this.url}/unreport`, {data : names});
  }
  reportGroupMarket(data: Omit<Market['exclusion'], 'excludeBy'|'isExclude'>&string[]): Subscribable<any>{
    return this.http.post(`${this.url}/report`, {data});
  }
}
