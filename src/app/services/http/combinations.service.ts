import {Injectable} from '@angular/core';
import {Subject, Subscribable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ICombination} from '../../definition/ICombination';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CombinationsService {

  combinationSubject = new Subject<ICombination[]>();
  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/bests`;

  constructor(private http: HttpClient) { }

  emitCombination(content: ICombination[]): void{
    this.combinationSubject.next(content);
  }

  getCombinations(): Subscribable<{combinations: ICombination[], usdToSpend}>{
    return this.http.get(`${this.url}/combinations`);
  }


  launchCombinationsCalculV2(): Subscribable<any>{
    return this.http.get(`${this.url}/calculV2`);
  }

}
