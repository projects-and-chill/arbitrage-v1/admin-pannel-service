import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Subject, Subscribable} from "rxjs";
import {Severity} from "../../definition/severity";
import {Reason} from "../../definition/reason";
import {Apikey} from "../../definition/apikey";
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CryptoService {


  constructor(private http : HttpClient) { }
  coinapiSubject = new Subject<Apikey>()
  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/crypto`;

  emmitCoinapi(content : Apikey) {
    this.coinapiSubject.next(content)
  }

  get_coinapi_infos() : void{
    this.http.get<{data : Apikey}>(`${this.url}/coinapi`).subscribe()
  }

  getSeverities() : Subscribable<{data : Severity[]}> {
    return this.http.get(`${this.url}/exclusion/severities`)
  }

  getReasons(queryParams = undefined) : Subscribable<{data : Reason[]}>{
    const obj = queryParams ? {for : queryParams} : {}
    return this.http.get(`${this.url}/exclusion/reasons`,{params : obj })
  }

  addReasons(body : Reason) : Subscribable<{data : Reason}> {
    return this.http.post(`${this.url}/exclusion/reasons`,body)
  }

  makeInit() : Subscribable<any>{
    return this.http.get(`${this.url}/init`)
  }

}
