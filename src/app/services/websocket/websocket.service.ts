import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  constructor() { }
  private socket$: WebSocketSubject<any>;
  public messagesSubject$ = new Subject();

  url = `ws://${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_WS_PATH}`;

  public connect(): void {
    if (!this.socket$ || this.socket$.closed) {
      this.socket$ = webSocket(this.url);
      this.socket$.subscribe({
        next:  msg => {this.messagesSubject$.next(msg); },
        error: (err) =>  console.log('Oups une erreur',  err),
        complete: () => this.socket$.unsubscribe(),
      });
    }else {
    }
  }

  sendMessage(msg: {event: string}&Record<string, any>): void {
    this.socket$.next(msg);
  }

  close(): any {
    this.socket$.complete();
  }
}
