import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject, Subscribable} from 'rxjs';
import {GraphConfig} from '../../definition/graphConfig';
import {Apikey} from '../../definition/apikey';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient  ) { }

  configSubject = new BehaviorSubject<{collapsed: boolean, theme: boolean}>({
    collapsed : false,
    theme : false
  });
  keysSubject = new Subject<Apikey[]>();
  url = `${location.protocol}//${location.hostname}:${environment.REVPROXY_PORT}/${environment.API_REST_PATH}/crypto`;

  isforSubject = new BehaviorSubject<GraphConfig>({
    isfor : 2000,
    START_GRAPH :  0, // Point de depart du graphique
    END_GRAPH:  20000, // Point de fin du graphique
    PAS_GRAPH:  50 // Saut entre chaque points du graphique
  });



  emmitConfig(content: {collapsed: boolean, theme: boolean}) {
    this.configSubject.next(content);
  }

  emmitIsfor(graphConfig: GraphConfig) {
    this.isforSubject.next(graphConfig);
  }

  emmitApikeys(keys: Apikey[]) {
    this.keysSubject.next(keys);
  }

  getKeys(): Subscribable<{data: Apikey[]}>{
    return this.http.get(`${this.url}/apikey`);
  }

  addKey(key: {key: string, mail?: string}): Subscribable<any>{
    return this.http.post(`${this.url}/apikey`, key);
  }

  removeApikey(key: string): Subscribable<any> {
    return this.http.delete(`${this.url}/apikey/${key}`);
  }

  chooseOtherKey(key: string): Subscribable<any>{
    return this.http.get(`${this.url}/apikey/choose/${key}`);
  }
  refreshApikey(key: string): Subscribable<any>{
    return this.http.get(`${this.url}/apikey/refresh/${key}`);
  }

  refreshAllApikeys(): Subscribable<any>{
    return this.http.get(`${this.url}/apikey/refresh`);
  }
}
