export interface ICombinationSide {
  symbol: string;
  market: string;
  price: number;
  volume: number;
}

export interface ICombination {
  id: string;
  name: string;
  pair: string;
  base: string;
  quote: string;
  volume: number;
  combinationGroup: number;
  usdToSpend: number;
  efficiency: number;
  timestamp: number;
  buy: ICombinationSide;
  sell: ICombinationSide;
}

export interface ICombinationSummary {
  id: string;
  name: string;
  pair: string;
  base: string;
  quote: string;
  volume: number;
  buy: ICombinationSide;
  sell: ICombinationSide;
  targetEfficiency: number;
  rentabilityDuration: number;
}

