export enum ClientEvent {
  start = 'start',
  stop = 'stop',
  updateComparationConfig = 'updateComparationConfig',
}

export enum ServerEvent {
  comparationConfig = 'comparationConfig',
  comparationState = 'comparationState',
  comparationExchanges = 'comparationExchanges',
  add = 'add',
  delete = 'delete',
  update = 'update',
  notification = 'notification',
}



export interface IComparationState {
  started: boolean;
  launching: boolean;
}

export interface IComparationConfig {
  cctxPair: string;
  baseUsd: number;
  minimumEfficiency: number;
  usdToSpend: number;
}
