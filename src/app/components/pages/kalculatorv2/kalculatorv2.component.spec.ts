import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Kalculatorv2Component } from './kalculatorv2.component';

describe('Kalculatorv2Component', () => {
  let component: Kalculatorv2Component;
  let fixture: ComponentFixture<Kalculatorv2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Kalculatorv2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Kalculatorv2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
