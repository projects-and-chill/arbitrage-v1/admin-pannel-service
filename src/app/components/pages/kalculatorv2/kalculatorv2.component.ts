import {Component, OnDestroy, OnInit} from '@angular/core';
import {ICombination} from '../../../definition/ICombination';
import {Subscription} from 'rxjs';
import {CombinationsService} from '../../../services/http/combinations.service';

@Component({
  selector: 'app-kalculatorv2',
  templateUrl: './kalculatorv2.component.html',
  styleUrls: ['./kalculatorv2.component.scss']
})
export class Kalculatorv2Component implements OnInit, OnDestroy {

  combinations: Array<ICombination> = [];
  usdToSpend: number;
  date: Date;
  private subscription: Subscription = new Subscription();

  constructor(
    private combinationService: CombinationsService
  ) { }

  ngOnInit(): void {
    this.subscription.add(this.combinationService.combinationSubject.subscribe((combinations: ICombination[]) => {
      this.combinations = combinations;
    } ));
    this.getCombinations();
  }


  calculBestsV2(): void{
    this.combinationService.launchCombinationsCalculV2().subscribe(() => {
      this.getCombinations();
    });
  }


  getCombinations(): void  {
    this.combinationService.getCombinations().subscribe((data) => {
        this.combinationService.emitCombination(data.combinations);
        this.usdToSpend = data.usdToSpend;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
