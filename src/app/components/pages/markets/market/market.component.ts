import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {MarketsService} from "../../../../services/http/markets.service";
import {ActivatedRoute, Router} from "@angular/router";
import {forkJoin, Observable, Subscribable, Subscription} from "rxjs";
import {map} from "rxjs/operators";
import {Pair} from "../../../../definition/pair";
import {CryptoService} from "../../../../services/http/crypto.service";
import {Market} from "../../../../definition/market";
import {Reason} from "../../../../definition/reason";
import {SymbolsService} from "../../../../services/http/symbols.service";
import {ISymbol, SymbolFor} from "../../../../definition/ISymbol";
import {Severity} from "../../../../definition/severity";
import {Asset} from "../../../../definition/asset";
import {ConfigService} from "../../../../services/autre/config.service";



interface SymbolPlus extends Omit<ISymbol, "pair">{
  pair : Pair
  data : Omit< SymbolFor['buy'|'sell'], "prixMoyen_quote">
}

interface MarketPlus extends Omit<Market, "exclusion" | "base" | "quote">{
  exclusion : Omit<Pair['exclusion'], "reasons"|"severity">&{
    severityPlus : Severity,
    reasonsPlus : Reason[]
  }
  base : Asset
  quote : Asset
}


@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit,OnDestroy {


  constructor(private cryptoServ : CryptoService,
              private marketsServ : MarketsService,
              private symbsServ : SymbolsService,
              private activatedRoute : ActivatedRoute,
              private router : Router,
              private configServ : ConfigService
  ) { }

  private subscription : Subscription = new Subscription()
  colors = ['green','default','gold','orange','red']
  colorsv2 = ['green','black','gold','orange','red']
  visible : boolean = false
  market : MarketPlus = undefined
  loading = true
  symbols : SymbolPlus[]
  pairsOn : number
  isfor : number
  paginate : {
    pageIndex : number
    pageSize : number
  } = {pageIndex : 1, pageSize : 10}
  list: {
    data : SymbolPlus[]
    sorter : string,
    maskOff : boolean,
    search : string
  } = {data : [], sorter : 'bestMarketFreq',maskOff : true, search : ''}
  dirty = false
  isDirty : EventEmitter<void> = new EventEmitter<void>()
  requestSymbs = [
    {$match: {market : this.activatedRoute.snapshot.paramMap.get('id')}},
    {$lookup: {from: "pairs", localField: "pair", foreignField: "name", as: "pair"}},
    {$unwind: "$pair"},
    {$sort: {"pair.name" : 1}},
  ]

  ngOnInit(): void {
    this.visible = true
    this.subscription.add(this.configServ.isforSubject.subscribe(({isfor}) => {
      this.isfor = isfor
      this.onUpdate()
    } ))
  }

  getMarket() : Subscribable<any>{
    return this.marketsServ.getMarkets( [
      {$match: {name : this.activatedRoute.snapshot.paramMap.get('id')}},
      {$lookup: {from: "severities",localField: "exclusion.severity",foreignField: "severity",as: "exclusion.severityPlus"}},
      {$lookup: {from: "reasons",localField: "exclusion.reasons",foreignField: "status",as: "exclusion.reasonsPlus"}},
      {$unwind: "$exclusion.severityPlus"}
    ])
  }

  onUpdate(dirty : boolean=false){
    if(dirty)
      this.dirty = true
    this.loading = true
    const $gethttp : Observable<{dataMarket: { data: MarketPlus[] }, dataSymbs : {data : SymbolPlus[]} }> = forkJoin(
      this.getMarket(),
      this.symbsServ.getSymbols(this.requestSymbs)
    ).pipe( // forkJoin returns an array of values, here we map those values to an object
      map(([dataMarket,dataSymbs])=>({dataMarket,dataSymbs})))

    $gethttp.subscribe(
      (resp) =>  {
        this.symbols = resp.dataSymbs.data
        this.pairsOn = this.symbols.filter(symb => symb.exclusion.isExclude === false && symb.pair.exclusion.isExclude === false).length
        this.market = resp.dataMarket.data[0]
        this.makeList()
        this.loading = false
      },
      ()=> null,
      ()=> this.loading = false
    )
  }

  makeList(sorter = this.list.sorter, search = this.list.search,isfor = this.isfor){
    const funcSort = (a,b) => {
      if(a.data[sorter] === undefined || a.data[sorter] === null)
        return 0
      if(b.data[sorter]  === undefined || b.data[sorter]  === null)
        return 0
      else
        return b.data[sorter]  - a.data[sorter]
    }

    let symbols : SymbolPlus[] = this.symbols.map(symb => {
      symb.data = {
        bestMarketFreq : symb.isfor[isfor].buy.bestMarketFreq + symb.isfor[isfor].sell.bestMarketFreq,
        okFreq: symb.isfor[isfor].buy.okFreq + symb.isfor[isfor].sell.okFreq,
        notDataFreq: symb.isfor[isfor].buy.notDataFreq + symb.isfor[isfor].sell.notDataFreq,
        notEnoughVolFreq: symb.isfor[isfor].buy.notEnoughVolFreq + symb.isfor[isfor].sell.notEnoughVolFreq,
      }
      return symb
    })
    this.list.data = symbols
    let listData = symbols.sort((a,b)=> funcSort(a,b) )
      .filter(symb => !this.list.maskOff || !(this.list.maskOff && (symb.exclusion.isExclude || symb.pair.exclusion.isExclude)) )
    if (search.length){
      if(search) this.list.search = search
      this.list.data = listData.filter(symb => new RegExp(`^${search}`,'i').test(symb.pair.name))
    }
    else{
      this.list.data = listData
    }
  }

  close(): void {
    this.visible = false
    const request = this.activatedRoute.snapshot.queryParamMap.get('request')
    if (this.dirty && request){
      const req = JSON.parse( request)
      this.symbsServ.getSymbols(req).subscribe(
        resp => {
          let marketPlus = resp ? resp.data.map(({market,pairsUsed}) => ({
            ...market,
            pairsUsed
          })) : []
          this.marketsServ.emmitMarkets(marketPlus)
        })
    }
    setTimeout(() => this.router.navigate(
      ['../'],
      { relativeTo: this.activatedRoute }), 250);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

}
