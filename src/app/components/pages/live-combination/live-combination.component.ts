import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebsocketService} from '../../../services/websocket/websocket.service';
import {interval, Subscription} from 'rxjs';
import {ICombination, ICombinationSummary} from '../../../definition/ICombination';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NzMessageService} from 'ng-zorro-antd';
import {ClientEvent, IComparationConfig, IComparationState, ServerEvent} from '../../../definition/IWsComparation';
import {ConfigService} from '../../../services/autre/config.service';
import {GraphConfig} from '../../../definition/graphConfig';

@Component({
  selector: 'app-live-combination',
  templateUrl: './live-combination.component.html',
  styleUrls: ['./live-combination.component.scss']
})
export class LiveCombinationComponent implements OnInit, OnDestroy {

  constructor(
    private websocketService: WebsocketService,
    private messsage: NzMessageService,
    private configServ: ConfigService,
  ) {}

  private parentSubscription: Subscription = new Subscription();

  public timers: Map<string, string> = new Map();
  public timerSubscriptions: Map<string, Subscription> = new Map();

  public profitableCombinations: ICombination[] = [];
  public previousProfitableCombinations: ICombinationSummary[] = [];

  public comparationState: IComparationState = {
    launching: true,
    started: false
  };

  public graphConfig: GraphConfig;
  public firstUpdateOccured = false;

  public exchangesMetadata: { exchanges: string[]; exchangeCompatibilities: string[] } = {
    exchanges: [],
    exchangeCompatibilities: []
  };

  public configComparationForm = new FormGroup({
    baseAsset: new FormControl( null , [Validators.required, Validators.minLength(3), Validators.pattern(/\w+/)]),
    quoteAsset: new FormControl(null, [Validators.required, Validators.minLength(3),  Validators.pattern(/\w+/)]),
    baseUsd: new FormControl(null, [Validators.required]),
    minimumEfficiency: new FormControl(null, [Validators.required]),
    usdToSpend: new FormControl(null, [Validators.required] ),
  });

  protected readonly JSON = JSON;


  ngOnInit(): void {
    this.websocketService.connect();
    const subscriptionGraphConfig = this.configServ.isforSubject.subscribe(graphConfig => {
      this.graphConfig = graphConfig;
      this.configComparationForm.patchValue({usdToSpend: graphConfig.isfor});
      if (this.firstUpdateOccured) {this.configComparationForm.markAsDirty(); }

    } );
    const subscriptionWebsocket = this.websocketService.messagesSubject$.subscribe((data: any) => this.handleData(data) );

    this.parentSubscription
      .add(subscriptionWebsocket)
      .add(subscriptionGraphConfig);


  }

  submitForm(): void {
    if (this.configComparationForm.status === 'INVALID') {
      this.messsage.create('warning', 'Formulaire invalide');
      return;
    }
    this.updateComparationConfig();
    this.configComparationForm.markAsPristine();
  }

  handleData(data: {event: string} & Record<string, any>): void {
    switch (data.event as ServerEvent){
      case ServerEvent.add:
        if (!this.profitableCombinations.some(({name}) => name === data.combination.name)) {
          const combination = data.combination;
          this.profitableCombinations.length
            ? this.profitableCombinations.push(combination)
            : this.profitableCombinations = [combination];
          this.createTimer(combination);
        }
        break;

      case ServerEvent.update :
        const combinationIndex = this.profitableCombinations.findIndex(({name}) => data.combinationName === name);
        if (combinationIndex === -1) {
          return;
        }
        this.profitableCombinations[combinationIndex].efficiency = data.efficiency;
        this.profitableCombinations[combinationIndex].timestamp = data.combinationTimestamp;
        break;

      case ServerEvent.delete :
        const combinationName = data.combinationName;
        const combinationIndex2 = this.profitableCombinations.findIndex(({name}) => combinationName === name);
        const duration = this.clearTimer(combinationName);
        if (combinationIndex2 === -1) {
          return;
        }

        const removedCombination = this.profitableCombinations.splice(combinationIndex2, 1)[0];
        if (this.previousProfitableCombinations.length > 20) {
          this.previousProfitableCombinations.shift();
        }

        const newItem = {
          ...removedCombination,
          targetEfficiency: data.targetEfficienty,
          rentabilityDuration: parseFloat(duration) || 0
        };

        if (!this.previousProfitableCombinations.length) {
          this.previousProfitableCombinations = [newItem];
        } else {
          this.previousProfitableCombinations.unshift(newItem);
        }
        break;

      case ServerEvent.comparationConfig:
        const {minimumEfficiency, baseUsd, usdToSpend, cctxPair} = data.comparationConfig as IComparationConfig;
        const [base, quote] = cctxPair.split('/');
        this.configComparationForm.patchValue({
          baseAsset: base,
          quoteAsset: quote,
          baseUsd,
          minimumEfficiency,
        });
        this.configServ.emmitIsfor({
          ...this.graphConfig,
          isfor: usdToSpend
        });
        this.firstUpdateOccured = true;
        break;
      case ServerEvent.comparationState:
        this.comparationState = data.state;
        break;
      case ServerEvent.notification:
        this.messsage.create(data.type, data.notification);
        break;
      case ServerEvent.comparationExchanges:
        const alphabeticSort = (wordA, wordB) => {
          if (wordA > wordB) {return 1; }
          else if (wordA < wordB) {return -1; }
          return 0;
        };
        this.exchangesMetadata.exchanges = data.exchanges.sort(alphabeticSort);
        this.exchangesMetadata.exchangeCompatibilities = data.exchangeCompatibilities.sort(alphabeticSort);
        break;

    }

  }

  startComparation(): void {
    this.websocketService.sendMessage({event: ClientEvent.start});
  }

  stopComparation(): void{
    this.websocketService.sendMessage({event: ClientEvent.stop});
  }

  updateComparationConfig(): void{
    const {  baseAsset, quoteAsset, baseUsd,  minimumEfficiency,  usdToSpend} = this.configComparationForm.value;
    const comparationConfig: IComparationConfig = {
      cctxPair: `${baseAsset.toUpperCase()}/${quoteAsset.toUpperCase()}`,
      minimumEfficiency,
      baseUsd,
      usdToSpend
    };
    this.websocketService.sendMessage({event: ClientEvent.updateComparationConfig, comparationConfig});
  }

  createTimer(combination: ICombination): void  {
    const timeSpendInSeconds =  (Date.now() - combination.timestamp) / 1000;
    this.timers.set(combination.name, timeSpendInSeconds.toFixed(1));
    const period = 300;
    const timerInterval = interval(period);
    const subscription = timerInterval.subscribe(() => {
      const currentTime = parseFloat(this.timers.get(combination.name));
      this.timers.set(combination.name, (currentTime + (period / 1000)).toFixed(1));
    });
    this.parentSubscription.add(subscription);
    this.timerSubscriptions.set(combination.name, subscription);
  }


  clearTimer(combinationName: string): string  {
    const subscription: Subscription | undefined = this.timerSubscriptions.get(combinationName);
    if (!combinationName) { return; }
    const timerDuration = this.timers.get(combinationName);
    subscription.unsubscribe();
    this.timerSubscriptions.delete(combinationName);
    this.timers.delete(combinationName);
    this.parentSubscription.remove(subscription);
    return timerDuration;
  }

  clearHistoric(): void {
    this.previousProfitableCombinations = [];
  }

  ngOnDestroy(): any {
    this.websocketService.close();
    this.parentSubscription.unsubscribe();
  }
}
