import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveCombinationComponent } from './live-combination.component';

describe('LiveCombinationComponent', () => {
  let component: LiveCombinationComponent;
  let fixture: ComponentFixture<LiveCombinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveCombinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveCombinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
