import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pair} from "../../../definition/pair";

@Component({
  selector: 'app-report-pairs',
  templateUrl: './report-pairs.component.html',
  styleUrls: ['./report-pairs.component.scss']
})
export class ReportPairsComponent implements OnInit {

  constructor() {}
  visible: boolean
  @Input()pairs : Pair[]
  @Input()selectMultiple : boolean = false
  @Output()
  afterUpdate : EventEmitter<boolean> = new EventEmitter<boolean>();

  ngOnInit(): void {
  }


}
