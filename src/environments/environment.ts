/**
 * Executer la commande suivante pour générer le fichier d'environnement "environment.ts" à partir de ce template:
 * ```bash
 * envsubst < ./src/environments/environment.template.ts > ./src/environments/environment.ts
 * ```
 */
export const environment = {
  production: true,
  REVPROXY_PORT: '80' || 80,
  API_REST_PATH: 'api1' || 'api1',
  API_WS_PATH: 'apiws' || 'apiws',
};

