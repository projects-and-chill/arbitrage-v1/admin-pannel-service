/**
 * Executer la commande suivante pour générer le fichier d'environnement "environment.ts" à partir de ce template:
 * ```bash
 * envsubst < ./src/environments/environment.template.ts > ./src/environments/environment.ts
 * ```
 */
export const environment = {
  production: true,
  REVPROXY_PORT: '$REVPROXY_PORT' || 80,
  API_REST_PATH: '$API_REST_PATH' || 'api1',
  API_WS_PATH: '$API_WS_PATH' || 'apiws',
};

