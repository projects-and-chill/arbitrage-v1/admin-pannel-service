FROM node:14.15.4
RUN \
  apt-get update \
  && apt-get -y install gettext-base \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*
RUN npm install -g @angular/cli@11.0.5

